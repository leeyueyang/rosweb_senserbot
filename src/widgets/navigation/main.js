var WidgetNavigation = function (widgetInstanceId) {
  // Mandatory properties
  var self = this;
  this.widgetInstanceId = widgetInstanceId;
  this.selector = ".jsWidgetContainer[data-widget-instance-id=" + self.widgetInstanceId + "]";
  
  // Global variables
  var setterFlag, viewer, nav, zoomViewer, poseScale;
  var toggleFlag = false;
  poseScale = 10;

  // Zooming and panning intervals
  var x_change,y_change,zoom;
  var originalX, originalY;
  var zoom = 1.1;

  this.clbkConfirm = function() {
    // Executed only once. 
    if(!setterFlag){
        setter();

        originalX = viewer.scene.x;
        originalY = viewer.scene.y;
        console.log("Original-X = "+ originalX + " original-y = "+ originalY);

        x_change = 10;
        y_change = 10;
        document.getElementById("x-change").innerHTML= "X-change: " + x_change.toPrecision(5);
        document.getElementById("y-change").innerHTML= "Y-change: " + y_change.toPrecision(5);

        //sets the 2 coordinates
        document.getElementById("x").innerHTML= "X-Coord: " + viewer.scene.x.toPrecision(5);
        document.getElementById("y").innerHTML= "Y-Coord: " + viewer.scene.y.toPrecision(5);

        setterFlag = true;
      }

      sliders();
  };
  this.clbkCreated = function() {
    var ros = new ROSLIB.Ros({ 
        ros: ros 
    }); ;

    // Setting Button
    $(document).delegate(this.selector + " .setter", 'click', (e) => {
      zoom = parseFloat(document.getElementById("zoom-slider").value);
      y_change = parseInt( document.getElementById("y-slider").value);
      x_change = parseInt( document.getElementById("x-slider").value);
      poseScale = parseFloat(document.getElementById("pose-slider").value);
      console.log("X-change: "+x_change + " Y-change: "+y_change + " Zoom: "+zoom + " Pose Scale: "+poseScale);

      document.getElementById("canvas").remove();
      setter();
    });

    $(document).keydown(function(){
      var key = event.which;

      console.log("X-change: "+x_change + " Y-change: "+y_change + " Zoom: "+zoom + " Pose Scale: "+poseScale);

      switch (key) {
        case 37:
          //left
          viewer.scene.x -= x_change;
          document.getElementById("x").innerHTML= "X-Coord: " + viewer.scene.x.toPrecision(5);
          break;
        case 38:
          //up
          viewer.scene.y -= y_change;
          document.getElementById("y").innerHTML= "Y-Coord: " + viewer.scene.y.toPrecision(5);
          break;
        case 40:
          //down
          viewer.scene.y += y_change;
          document.getElementById("y").innerHTML= "Y-Coord: " + viewer.scene.y.toPrecision(5);
          break;
        case 39:
          //right
          viewer.scene.x = viewer.scene.x + x_change; 
          document.getElementById("x").innerHTML= "X-Coord: " + viewer.scene.x.toPrecision(5);
          break;
        case 189:
          //zoom out
          viewer.scene.scaleX /= zoom; 
          viewer.scene.scaleY /= zoom;

          document.getElementById("x").innerHTML= "X-Coord: " + viewer.scene.x.toPrecision(5);
          document.getElementById("y").innerHTML= "Y-Coord: " + viewer.scene.y.toPrecision(5);
          break;
        case 187:
          //zoom in
          viewer.scene.scaleX *= zoom; 
          viewer.scene.scaleY *= zoom;

          document.getElementById("x").innerHTML= "X-Coord: " + viewer.scene.x.toPrecision(5);
          document.getElementById("y").innerHTML= "Y-Coord: " + viewer.scene.y.toPrecision(5);
          break;
        default:
          console.log(key + " was pressed");
      }
    });

    setterFlag = false;
  };
  this.clbkResized = function(width, height) {
    self.width = width;
    self.height = height;
    if (self.viewer != null) {
      self.viewer.width = width;
      self.viewer.height = height;
    }
    $("#" + self.viewerElement + " canvas").attr({
      width: width,
      height: height
    });
  };
  this.clbkMoved = function(x,y) {};
  this.clbkTab = function(isMyTab) {};

  // Functions
  var setter = function() {
    viewer = new ROS2D.Viewer({ 
        divID : 'nav', 
        width : 350, 
        height : 350 
    }); 
    
    // Setup the nav client. 
    nav = NAV2D.OccupancyGridClientNav({ 
        ros : ros, 
        rootObject : viewer.scene, 
        viewer : viewer, 
        serverName : '/move_base' ,
        poseScale: poseScale
    }); 

    //sets the 2 coordinates
    document.getElementById("x").innerHTML= "X-Coord: " + viewer.scene.x.toPrecision(5);
    document.getElementById("y").innerHTML= "Y-Coord: " + viewer.scene.y.toPrecision(5);
  };

  var sliders = function() {
    document.getElementById("x-slider").onmouseup = function(){
      document.getElementById("x-change").innerHTML= "X-change: " + parseInt( document.getElementById("x-slider").value);;
    };

    document.getElementById("y-slider").onmouseup = function(){
      document.getElementById("y-change").innerHTML= "Y-change: " + parseInt( document.getElementById("y-slider").value);;
    };
  
    document.getElementById("zoom-slider").onmouseup = function(){
      console.log("Zoom Slider is: " + parseFloat( document.getElementById("zoom-slider").value));
    };

    document.getElementById("pose-slider").onmouseup = function(){
      console.log("Pose Scale Slider is: " + parseFloat( document.getElementById("pose-slider").value));
    };
  }

}