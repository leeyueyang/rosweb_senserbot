///<reference path="../../ts/typings/tsd.d.ts" />

import { WidgetParent } from '../../ts/classmodel/widget'
import { Helper } from '../../ts/helpers/html';

declare var ros: ROSLIB.Ros;

class WidgetTeleops extends WidgetParent {
  public widgetInstanceId: number;
  public selector: string;

    // ===== widget params =====
  public topicName: string;
  public topicType: string;
  public topic: ROSLIB.Topic;
  private boolFlag: boolean;  //boolFlag represents whether publisher should operate

    // ===== publisher params =====
    public globX: number = 0;
    public globY: number = 0;
    public globZ: number = 0;
    public speedMultiplier: number = 1.0;

  constructor(widgetInstanceId: number) {
      super(widgetInstanceId);

      //most likely unnecessary
      this.topic = new ROSLIB.Topic({ ros: ros, name: "", messageType: "" });
  }

  clbkCreated(): void {
      this.boolFlag = false;
      
      //Delegates button to run this function
      $(document).delegate(this.selector + " .jsTeleops", 'click', (e) => {
        this.keylogger();
    });   
      

      $(document).delegate(this.selector + " .jsSpeed", 'click', (e) => {
        this.setter();
    });
  }

  clbkResized(): void {
  }

  clbkMoved(): void {
  }

  clbkTab(): void  {
      console.log("Tab Changed");
      this.boolFlag = false;
      this.stateChecker(this.boolFlag);
  }

  clbkConfirm(): void{
       this.stateChecker(this.boolFlag);
  }

  //button functions
  alerter(): void{
      alert("Debugging message");
  }

  setter(): void{
    var input = $(this.selector).find("input[type=number]");
    var value: number = parseFloat($(input).val());

    //alert("Multipler is "+value);

    if (value <= 0 || value > 10 || this.globX !== 0 || this.globY !== 0 || this.globZ !== 0){
      $(this.selector).find("input[type=number]").attr("style", "color: red");
      if(value <= 0 || value > 10){
        alert("Invalid values, min val = greater than 0, max val = 10");
      }
    }
    else{
      this.speedMultiplier = value/10;
      $(this.selector).find("input[type=number]").attr("style", "color: black");
    }
  }

  keylogger(): void{
    this.boolFlag = !this.boolFlag;
    this.stateChecker(this.boolFlag);
  }

  stateChecker(boolFlag: boolean): void{
    if (boolFlag) {
      this.visualChanges(true);

      
      //assigns the event listeners, when these keys are pressed, they are logged
      $(document).delegate($(document),"keydown", (e) => {
        this.keyAssigner(event);
    });   

      $(document).delegate($(document),"keyup", (e) => {
        this.keyRemover(event);
      }); 
    }
    else{
      this.visualChanges(false);

      //detach the event listeners
      $(document).off("keydown keyup");
    }
  }

  keyAssigner(event): void{

    //params required       //for us to compare the old vs new values, if no change don't create new publisher
    var oldX :number = this.globX;
    var oldY :number = this.globY;
    var oldZ :number = this.globZ;
    

    var key: number;
    key = event.which;  //find another way to get this code, maybe get the on method to pass the code works too

    //Checks for different keys, highlights which keys are being pressed
    switch(key){
      case 81:  //strafe left
        console.log("Q" + " was pressed");

        if(this.globY >= 0){  //if the other key is not already pressed
          $(document).find(" #Q").attr("style", "background-color: peachpuff");
          this.globY = 0.5 * this.speedMultiplier;
        }
        break;

      case 87:  //up
        console.log("W" + " was pressed");

        if(this.globX >= 0){
          this.globX = 0.5 * this.speedMultiplier;
          $(document).find(" #W").attr("style", "background-color: peachpuff");
        }
        break;

      case 69:  //strafe right
        console.log("E" + " was pressed");

        if(this.globY <= 0){
          this.globY = -0.5 * this.speedMultiplier;
          $(document).find(" #E").attr("style", "background-color: peachpuff");
        }
        break;

      case 65:  //turn left
        console.log("A" + " was pressed");

        if(this.globZ >= 0){
          this.globZ = 1 * this.speedMultiplier;
          $(document).find(" #A").attr("style", "background-color: peachpuff");
        }
        break;

      case 83:  //down
        console.log("S" + " was pressed");

        if(this.globX <= 0){
          this.globX = -0.5 * this.speedMultiplier;
          $(document).find(" #S").attr("style", "background-color: peachpuff");
        }
        break;

      case 68:  //turn right
        console.log("D" + " was pressed");

        if(this.globZ <= 0){
          this.globZ = -1 * this.speedMultiplier;
          $(document).find(" #D").attr("style", "background-color: peachpuff");
        }
        break;

      default:
        console.log(key + "was pressed");
    }

      let name: string = "cmdVel";  //topic name     
      
      //outlines message of the publisher
      let message = new ROSLIB.Message({
        angular : {
          x : 0,
          y : 0,
          z : this.globZ
        },
        linear : {
          x : this.globX,
          y : this.globY,
          z : this.globZ
        }
      });

      //change this to where publisher should be

      if(this.globX !== oldX || this.globY !== oldY || this.globZ !== oldZ){
        console.log("Publisher attached");
        this.publish(message);
      }
  }

  keyRemover(event): void{
          var key: number;
          key = event.which;

          var oldX :number = this.globX;
          var oldY :number = this.globY;
          var oldZ :number = this.globZ;

          switch(key){
            case 81:
              console.log("Q" + " was released");

              if(this.globY > 0){
                $(document).find(" #Q").attr("style", "background-color: white");
                this.globY +=  -0.5 * this.speedMultiplier;
              }
              break;
            case 87:
            console.log("W" + " was released");

              if(this.globX > 0){
                $(document).find(" #W").attr("style", "background-color: white");
                this.globX += -0.5 * this.speedMultiplier;
              }
              break;
            case 69:
              console.log("E" + " was released");

              if(this.globY < 0){
                $(document).find(" #E").attr("style", "background-color: white");
                this.globY += 0.5 * this.speedMultiplier;
              }
              break;
            case 65:
              console.log("A" + " was released");

              if(this.globZ > 0){
                $(document).find(" #A").attr("style", "background-color: white");
                this.globZ += -1 * this.speedMultiplier;
              }
              break;
            case 83:
              console.log("S" + " was released");

              if(this.globX < 0){
                $(document).find(" #S").attr("style", "background-color: white");
                this.globX += 0.5 * this.speedMultiplier;
              }
              break;
            case 68:
              console.log("D" + " was released");

              if(this.globZ < 0){
                $(document).find(" #D").attr("style", "background-color: white");
                this.globZ += 1 * this.speedMultiplier;
              }
              break;
            default:
              console.log(key + "was released");
          }

          let message = new ROSLIB.Message({
              angular : {
                x : 0,
                y : 0,
                z : this.globZ
            },
              linear : {
                x : this.globX,
                y : this.globY,
                z : this.globZ
            }
        });

          let publisher = new ROSLIB.Topic({    
          ros: ros,
          name: "cmdVel", //whichever topic name the user wants to fill in
          messageType: 'geometry_msgs/Twist'
        });

        if(this.globX !== oldX || this.globY !== oldY || this.globZ !== oldZ){
          publisher.publish(message);
        }
  }


  visualChanges(flag: boolean): void{
      if(flag){
        //executed during keydown
        $(this.selector).find(" .indicator").attr("style", "color: limegreen");
        $(this.selector).find(" .indicator").text("Status: On");
      }
      else{
        //executed during keyup
        $(this.selector).find(" .indicator").attr("style", "color: red");
        $(this.selector).find(" .indicator").text("Status: Off");
      }
  }

  public interval: any;
  publish(message: ROSLIB.Message): void {
      //let name: string = this.topicName;        //placeholder for topicName
      let name: string = "cmdVel";
      
      let publisher = new ROSLIB.Topic({    
        ros: ros,
        name: name, //whichever topic name the user wants to fill in
        messageType: 'geometry_msgs/Twist'
      });

      //this is where the publishing is done
      publisher.publish(message);
  }
}

//Adds this widget towards the whole list of launchable windows
window["WidgetTeleops"] = WidgetTeleops;


